**FilePathTool** is a simple C++ class that makes simple operations with file paths.  
It uses wstring, so it's utf-16.  
Is should be OS indipendent.  

**Log**:

- **v0.2** added removeFileName
  Features:
  
    - get the file extension
    - get the path without the extension
    - get the file name
    - get the file directory path

- **v0.1** Works under linux. (dirs separator = '/')  
  Features:
  
    - get the file extension
    - get the path without the extension
    - get the file name
