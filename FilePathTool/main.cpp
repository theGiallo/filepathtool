/* 
 * File:   main.cpp
 * Author: thegiallo
 *
 * Created on 18 aprile 2013, 20.06
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include "FilePathTool.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    wstring path(L"/home/user/folder/subfolder/file.ext");
    wcout<<"Testing FilePathTool"<<endl<<endl;
    wcout<<"path = "<<path<<endl;
    wcout<<"ext = "<<FilePathTool::getFileExtension(path)<<endl;
    wcout<<"no-ext = "<<FilePathTool::removeFileExtension(path)<<endl;
    wcout<<"name = "<<FilePathTool::getFileName(path)<<endl;
    wcout<<"name-no-ext = "<<FilePathTool::removeFileExtension(FilePathTool::getFileName(path))<<endl;
    wcout<<"file-dir-path = "<<FilePathTool::removeFileName(path)<<endl;
    
    wcout<<endl;
    path = L"/home/user/folder/@ẃé¶ŧýúíóþáßðđŋħjĸł«»ç“”nµ/filewithoutextinthename";
    wcout<<"path = "<<path<<endl;
    wcout<<"ext = "<<FilePathTool::getFileExtension(path)<<endl;
    wcout<<"no-ext = "<<FilePathTool::removeFileExtension(path)<<endl;
    wcout<<"name = "<<FilePathTool::getFileName(path)<<endl;
    wcout<<"name-no-ext = "<<FilePathTool::removeFileExtension(FilePathTool::getFileName(path))<<endl;
    wcout<<"file-dir-path = "<<FilePathTool::removeFileName(path)<<endl;
    return 0;
}

