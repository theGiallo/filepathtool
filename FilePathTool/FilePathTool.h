/* 
 * File:   FilePathTool.h
 * Author: thegiallo
 *
 * Created on 18 aprile 2013, 20.08
 */

#include <string>

#ifndef FILEPATHTOOL_H
#define	FILEPATHTOOL_H

class FilePathTool
{
public:
    FilePathTool();
    FilePathTool(const FilePathTool& orig);
    virtual ~FilePathTool();
    static std::wstring getFileExtension(const std::wstring& file_path);
    static std::wstring removeFileExtension(const std::wstring& file_path);
    static std::wstring removeFileName(const std::wstring& file_path);
    static std::wstring getFileName(const std::wstring& file_path);
    
    static const wchar_t DIR_SEPARATOR = '/';
    
private:

};

#endif	/* FILEPATHTOOL_H */

