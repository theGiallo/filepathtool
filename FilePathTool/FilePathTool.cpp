/* 
 * File:   FilePathTool.cpp
 * Author: thegiallo
 * 
 * Created on 18 aprile 2013, 20.08
 */

#include "FilePathTool.h"

FilePathTool::FilePathTool()
{
}

FilePathTool::FilePathTool(const FilePathTool& orig)
{
}

FilePathTool::~FilePathTool()
{
}

std::wstring FilePathTool::getFileExtension(const std::wstring& file_path)
{
    std::wstring ext = L"";
    wchar_t ext_sep = L'.'; 
    for (std::wstring::const_reverse_iterator cit = file_path.rbegin(); cit!=file_path.rend() ; cit++)
    {
        if (*cit == ext_sep)
        {
            return ext;
        }
        if (*cit == DIR_SEPARATOR)
        {
            return L"";
        }
        std::wstring tmpstr;
        tmpstr.push_back(*cit);
        ext=tmpstr+ext;
    }
    return ext;
}

std::wstring FilePathTool::removeFileExtension(const std::wstring& file_path)
{
    int l = getFileExtension(file_path).length();
    if (l!=0)
    {
        l++;
    }
    return file_path.substr(0,file_path.length()-l);
}

std::wstring FilePathTool::getFileName(const std::wstring& file_path)
{
    std::wstring name = L"";
    for (std::wstring::const_reverse_iterator cit = file_path.rbegin(); cit!=file_path.rend() ; cit++)
    {
        if (*cit == DIR_SEPARATOR)
        {
            return name;
        }
        std::wstring tmpstr;
        tmpstr.push_back(*cit);
        name=tmpstr+name;
    }
    return name;
}

std::wstring FilePathTool::removeFileName(const std::wstring& file_path)
{
    int l = getFileName(file_path).length();
    return file_path.substr(0,file_path.length()-l);
}